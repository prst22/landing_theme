<?php /* Template Name: All standard asides */ ?>

<?php get_header();?>
<?php
    $term = get_term(get_the_ID());

    $paged = ( get_query_var( 'paged' ) ) ? absint( get_query_var( 'paged' ) ) : 1;
    
    $standard_aside_args = array(
        'post_status' => 'publish',
        'fields' => 'ids',
        'paged' => $paged,
        'tax_query' => array(
            array(                
                'taxonomy' => 'post_format',
                'field' => 'slug',
                'terms' => array( 
                    'post-format-video',
                    'post-format-gallery'
                ),
                'operator' => 'NOT IN'
            )
        )
    );
    ?>
    <main class="container grid-xl main">
        <div class="columns">
        <?php 
            $standard_aside_query = new WP_Query($standard_aside_args); ?>
            <?php if(get_theme_mod('front_page_blog_title')): ?>

                <h1 class="column col-12 text_center">
                    <?php echo get_theme_mod('front_page_blog_title'); ?>
                </h1>

            <?php endif; ?>
            <?php if ($standard_aside_query->have_posts()):?>
                <?php while ($standard_aside_query->have_posts()):

                    $standard_aside_query->the_post();
                    
                    if(get_post_format() === 'aside'): ?>

                        <?php get_template_part( 'template_parts/index', get_post_format() ); ?>

                    <?php else: ?>

                        <?php get_template_part( 'template_parts/index',  'standard' ); ?>

                    <?php endif; ?>

                <?php endwhile; ?>

            <?php wp_reset_postdata();?> 
                        
            <?php endif; ?>

            <?php if($standard_aside_query->max_num_pages > 0): ?>
                <div class="column col-12">
                    <div class="pagination">
                        <?php echo paginate_links(
                            array(
                                'prev_next'     => false,
                                'total'         => $standard_aside_query->max_num_pages,
					            'current'       => $paged,
                                'type'          => 'list',
                                'end_size'      => 2,
                                'mid_size'      => 2
                                )
                            ); 
                        ?>
				    </div>
                </div>
            <?php endif;?>
            
        </div>
    </main>
<?php get_footer();?>