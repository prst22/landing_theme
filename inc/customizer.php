<?php  
	function landing_customizer_register($wp_customize){
		$wp_customize->add_section('front_page_section_title', array(
			'title'				=> __('Front page section title', 'landing_theme'),
			'description'		=> sprintf(__('Front page news title', 'landing_theme')),
			'priority'			=> 10
        ));
        $wp_customize->add_setting('front_page_video_section_title', array(
			'default'		=> _x('Video galleries', 'landing_theme'),
			'type' 			=> 'theme_mod'
		));
        $wp_customize->add_control('front_page_video_section_title', array(
			'label'			=> __('Video section title', 'landing_theme'),
			'section'		=> 'front_page_section_title',
			'priority' 		=> 1
		));
        $wp_customize->add_setting('front_page_blog_title', array(
			'default'		=> _x('News', 'landing_theme'),
			'type' 			=> 'theme_mod'
		));
        $wp_customize->add_control('front_page_blog_title', array(
			'label'			=> __('Blog section title', 'landing_theme'),
			'section'		=> 'front_page_section_title',
			'priority' 		=> 2
		));
	}
	add_action('customize_register', 'landing_customizer_register');