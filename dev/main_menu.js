import { wavesAndDucksTl } from './animations';

const menuBtn = document.getElementById('mobile_menu_btn'),
closeMenuBtn = document.getElementById('close_menu_btn'),
mobileMenu = document.querySelector('.mobile_menu_cnt'),
mobileMenuList = document.querySelector('.mobile_menu_cnt__inner'),
megaMenuTrigger = document.querySelectorAll('.top_menu .dropdown_toggle'),
mobileMegaMenuTrigger = document.querySelectorAll('.mobile_menu_list_cnt__mobile_list .dropdown_toggle'),
dropDownMenuCnt = document.querySelectorAll('.top_menu .dropdown-menu.depth_0'),
desktopAnimationCnt = document.querySelector('.desktop_top_animation');
let initialdropDownTrigger,
x = window.matchMedia("(min-width: 961px)"),
openMenuTween = Object.create(null),
closeMenuTween = Object.create(null);

// mega menu start
function closeAllDropDowns(dropDown){
    if(desktopAnimationCnt) wavesAndDucksTl.resume();
    document.body.classList.remove('dropdownmenu_active');
    if('isActive' in openMenuTween){
        openMenuTween.kill();
    }
    if('isActive' in closeMenuTween){
        closeMenuTween.kill();
    }
    dropDown.forEach( dropdown_item => {
        if(dropdown_item){
            closeMenuTween = new TweenMax.to(dropdown_item, 1.2, { opacity: 0, x: 30, scale: 0.98, delay: 0, ease: Back.easeOut.config(3) });
            dropdown_item.classList.remove('pointer_events');
        }
    }); 
} 

function animateMenuDropDown(e, withDelay =0){
    e.currentTarget.nextElementSibling.classList.add('pointer_events');
    document.body.classList.add('dropdownmenu_active');
    openMenuTween = new TweenMax.to(e.currentTarget.nextElementSibling, 0.7, { opacity: 1, x: 0, scale: 1, force3D: true, delay: withDelay, ease: Back.easeOut.config(3) });
}

function toggleMegaMenu(e, isEscape =false){
    e.preventDefault();
    
    function openMenu(){
        if(!dropDownMenuCnt){
            return
        }
        if(desktopAnimationCnt) wavesAndDucksTl.pause();
        if(isEscape){
            closeAllDropDowns(dropDownMenuCnt);
            
        }else{
            const isMegaMenuActive = document.querySelector('.dropdown-menu.pointer_events');
            if(isMegaMenuActive){
                closeAllDropDowns(dropDownMenuCnt);
                if(initialdropDownTrigger === e.currentTarget.nextElementSibling){
                    closeAllDropDowns(dropDownMenuCnt);
                }else{
                    animateMenuDropDown(e, 0.3);
                    if(desktopAnimationCnt) wavesAndDucksTl.pause();
                }
            }else{
                animateMenuDropDown(e);
            }
            initialdropDownTrigger = e.currentTarget.nextElementSibling;
        }  
    }
    openMenu();  
}

if(megaMenuTrigger.length > 0 && dropDownMenuCnt.length > 0){
    megaMenuTrigger.forEach((element) => {
        element.addEventListener('click', toggleMegaMenu);
    });
    // close drop down on click out of the drop down menu start
    window.addEventListener('click', (e) => {
        if(x.matches && !e.target.matches('.top_menu .dropdown-menu') 
            && !e.target.matches('.top_menu .dropdown_toggle')
            && !e.target.matches('.top_menu .dropdown_toggle *')
            && !e.target.matches('.top_menu .dropdown-menu *')){
                closeAllDropDowns(dropDownMenuCnt);
        }
    });
    // close drop down on click out of the drop down menu end
}
// mega menu end

// mobile menu start

if(menuBtn && mobileMenuList){
    menuBtn.addEventListener('click', toggleMobileMenu);
    closeMenuBtn.addEventListener('click', toggleMobileMenu);
    if(mobileMegaMenuTrigger.length > 0){
        mobileMegaMenuTrigger.forEach( dropdown_item => {
            dropdown_item.addEventListener('click', openMobileDropDown);
        });
    }
    // close mobile menu on click out of the drop down menu start
    window.addEventListener('click', (e) => {
        if(!x.matches && document.body.classList.contains('mobile_menu_active') 
            && !e.target.matches('.mobile_menu_cnt__inner') 
            && !e.target.matches('#mobile_menu_btn')
            && !e.target.matches('#mobile_menu_btn *')
            && !e.target.matches('.mobile_menu_cnt__inner *')){
                toggleMobileMenu(e); 
        }
    });
    // close mobile menu on click out of the drop down menu end
}

function controllVpSize(x){
    if (x.matches && document.body.classList.contains('mobile_menu_active')){ // If media query matches
        toggleMobileMenu(); 
    }
    if(!x.matches && document.body.classList.contains('dropdownmenu_active')){ 
        closeAllDropDowns(dropDownMenuCnt);
    } 
}

controllVpSize(x); // Call listener function at run time
x.addListener(controllVpSize);
document.addEventListener('keydown', closeMenuOnEsc);

function toggleMobileMenu(e){
    function openCloseMenuWithNoTransitionDelay(){
        //check if top animation is running start
        if(desktopAnimationCnt) wavesAndDucksTl.isActive() ? wavesAndDucksTl.pause() : wavesAndDucksTl.resume();
        //check if top animation is running end
        mobileMenu.classList.remove('transition_delay');
        document.body.classList.toggle('mobile_menu_active');
        mobileMenu.classList.toggle('hidden');
        mobileMenuList.classList.toggle('hidden');
    }
    if(e){ //check event to target click 
        if(e.target.closest('button') === closeMenuBtn 
            || e.target.matches('.mobile_menu_cnt')
            || e.key == "Escape" 
            || e.key == "Esc" 
            || e.keyCode == 27){ //choose open or close or esc button to determin whether to use transition delay
                //check if top animation is running start
                if(desktopAnimationCnt) wavesAndDucksTl.isActive() ? wavesAndDucksTl.pause() : wavesAndDucksTl.resume();
                //check if top animation is running end
                document.body.classList.toggle('mobile_menu_active');
                mobileMenu.classList.add('transition_delay');
                mobileMenu.classList.toggle('hidden');
                mobileMenuList.classList.toggle('hidden');
        }else{
            openCloseMenuWithNoTransitionDelay();
        }
    }else{
        openCloseMenuWithNoTransitionDelay();
    }
}

function closeMenuOnEsc(evt){
    evt = evt || window.event;
    let isEscape = false,
    bodyClasses = document.body.classList;
    if("key" in evt){
        isEscape = (evt.key == "Escape" || evt.key == "Esc");
    }else{
        isEscape = (evt.keyCode == 27);
    }
    if(isEscape && bodyClasses.contains('mobile_menu_active')){
        toggleMobileMenu(evt);
    }
    if(isEscape && bodyClasses.contains('dropdownmenu_active')){
        toggleMegaMenu(evt, true);
    }
}

function whichTransitionEvent(){
    let t,
    el = document.createElement("fakeelement"),
    transitions = {
        "transition"      : "transitionend",
        "OTransition"     : "oTransitionEnd",
        "MozTransition"   : "transitionend",
        "WebkitTransition": "webkitTransitionEnd"
    }

    for (t in transitions){
        if (el.style[t] !== undefined){
            return transitions[t];
        }
    }
}

let transitionEvent = whichTransitionEvent();

function openMobileDropDown(e){
    e.preventDefault();
    const mobileDropdown = e.currentTarget.nextElementSibling;
    if(!mobileDropdown.classList.contains('dropdown_is_visible')){
        TweenMax.set(mobileDropdown, { overflow: 'visible', maxHeight: 9999, className: '+=dropdown_is_visible' });
        TweenMax.to(mobileDropdown, 0.3, { opacity: 1, force3D: true, ease: Power0.easeNone });
    }else{
        TweenMax.to(mobileDropdown, 0.3, { opacity: 0, force3D: true, ease: Power0.easeNone, onComplete: function(){
            TweenMax.set(mobileDropdown, { overflow: 'hidden', maxHeight: 0, className: '-=dropdown_is_visible' });
        } });
    }  
    
}
// mobile menu end

