import TweenMax from "gsap/TweenMax";
import { ScrollToPlugin } from "gsap/all";
import { ScrambleTextPlugin } from "gsap/all";
import { TextPlugin } from "gsap/TextPlugin.js";

const plugins = [ ScrollToPlugin, ScrambleTextPlugin, TextPlugin ];
import lazySizes from 'lazysizes';
import 'lazysizes/plugins/parent-fit/ls.parent-fit';
// polyfills
import 'lazysizes/plugins/respimg/ls.respimg';
if (!('object-fit' in document.createElement('a').style)){
    require('lazysizes/plugins/object-fit/ls.object-fit');
}
import 'lazysizes/plugins/blur-up/ls.blur-up';

import GLightbox from 'glightbox';

import animate from './animations';
import { wavesAndDucksTl } from './animations';

import './main_menu';

const lightbox = GLightbox({
    touchNavigation: true,
    slideEffect: 'fade',
    loopAtEnd: true,
    autoplayVideos: true,
    svg:{
        close: '<svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="95.939px" height="95.939px" viewBox="0 0 95.939 95.939" style="enable-background:new 0 0 95.939 95.939;" xml:space="preserve"><g><path style="fill:#FFFFFF;" d="M62.819,47.97l32.533-32.534c0.781-0.781,0.781-2.047,0-2.828L83.333,0.586C82.958,0.211,82.448,0,81.919,0c-0.53,0-1.039,0.211-1.414,0.586L47.97,33.121L15.435,0.586c-0.75-0.75-2.078-0.75-2.828,0L0.587,12.608c-0.781,0.781-0.781,2.047,0,2.828L33.121,47.97L0.587,80.504c-0.781,0.781-0.781,2.047,0,2.828l12.02,12.021c0.375,0.375,0.884,0.586,1.414,0.586c0.53,0,1.039-0.211,1.414-0.586L47.97,62.818l32.535,32.535c0.375,0.375,0.884,0.586,1.414,0.586c0.529,0,1.039-0.211,1.414-0.586l12.02-12.021c0.781-0.781,0.781-2.048,0-2.828L62.819,47.97z"/></g></svg>',
        next: '<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 540 540" xml:space="preserve"><g><path d="M4.1,536.6l503.5-249.1l28.4-14.3l-28.4-14.3l-480-240L4.5,7v26.2v192h96v-48c0-5.5,0.4-24.6,0.4-24.6s17.8,7.9,22.7,10.3l192,96l30.2,14.3l-30.1,14.3l-192,96l0,0l-23.8,10.2c0,0,0.6-19,0.6-24.5v-48h-96v192L4.1,536.6"/></g></svg>',
        prev: '<svg version="1.1" id="Layer_2" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 540 540" style="enable-background:new 0 0 540 540;" xml:space="preserve"><g><path d="M536.5,7.5l-504,248.1L4.1,269.9l28.4,14.4l479.5,241l23.1,11.9l0.1-26.2l0.4-192l-96-0.2l-0.1,48c0,5.5-0.4,24.6-0.4,24.6s-17.8-7.9-22.7-10.3l-191.8-96.4l-30.2-14.4l30.1-14.2l192.2-95.6l0,0l23.8-10.2c0,0-0.6,19-0.6,24.5l-0.1,48l96,0.2l0.4-192L536.5,7.5"/></g></svg>'
    },
}),
desktopAnimationCnt = document.querySelector('.desktop_top_animation');
window.lazySizesConfig = window.lazySizesConfig || {};
//page is optimized for fast onload event
lazySizesConfig.loadMode = 1;

// underscore js part start
let o = function(obj) {
    if (obj instanceof o) return obj;
    if (!(this instanceof o)) return new o(obj);
    this.owrapped = obj;
};
o.now = Date.now || function() {
    return new Date().getTime();
};
o.throttle = function(func, wait, options) {
    let timeout, context, args, result;
    let previous = 0;
    if (!options) options = {};

    let later = function() {
        previous = options.leading === false ? 0 : o.now();
        timeout = null;
        result = func.apply(context, args);
        if (!timeout) context = args = null;
    };

    let throttled = function() {
        let now = o.now();
        if (!previous && options.leading === false) previous = now;
        let remaining = wait - (now - previous);
        context = this;
        args = arguments;
        if (remaining <= 0 || remaining > wait) {
        if (timeout) {
            clearTimeout(timeout);
            timeout = null;
        }
        previous = now;
        result = func.apply(context, args);
        if (!timeout) context = args = null;
        } else if (!timeout && options.trailing !== false) {
            timeout = setTimeout(later, remaining);
        }
        return result;
    };

    throttled.cancel = function() {
        clearTimeout(timeout);
        previous = 0;
        timeout = context = args = null;
    };

    return throttled;
};

// underscore js part end

// for each polyfill start

if (window.NodeList && !NodeList.prototype.forEach){
    NodeList.prototype.forEach = function (callback, thisArg){
        thisArg = thisArg || window;
        for(var i = 0; i < this.length; i++){
            callback.call(thisArg, this[i], i, this);
        }
    };
}

// for each polyfill end
let goTopBtn = document.getElementById('go_top'),
throttledRotation = o.throttle(function(e){
    animate.handleMouseMove.call(animate, e)
}, 70, { leading: false, trailing: false });

if(document.body.classList.contains('home') 
    || document.body.classList.contains('page-template-page-all-videos') 
    || document.body.classList.contains('page-template-page-all-galleries')){
        const galleryThumbs = document.querySelectorAll('.gallery_item'),
        galleryLink = document.querySelectorAll('.gallery_item--image_gallery'),
        galleryGlare = document.querySelectorAll('.glare');

        if(galleryLink){
            galleryLink.forEach( (link) => {
                link.addEventListener('click', (e) => {
                    e.preventDefault();
                });
            });
        }
        if(galleryThumbs){
            galleryThumbs.forEach( (thum) => {
                thum.addEventListener('mouseenter', animate.handleMouseEnter.bind(animate));
                thum.addEventListener('mousemove', (e) => {
                    throttledRotation(e);
                });
                thum.addEventListener('mouseleave', animate.handleMouseLeave.bind(animate));        
            });
        }
        if(galleryGlare){
            galleryGlare.forEach( (g) => {
                g.addEventListener('mousemove', animate.initGlare);
                g.addEventListener('mouseenter', animate.removeGlare);
                g.addEventListener('mouseleave', animate.removeGlare);
            });
        }

}

if(goTopBtn){
    goTopBtn.addEventListener('click', (e) => {
        e.preventDefault();
        if(typeof scrOffsetTop !== 'undefined'){
            TweenMax.to(window, 2, { scrollTo: { y: ".nav_bar", offsetY: scrOffsetTop.offsetY }, ease: Power2.easeOut });
        }else{
            TweenMax.to(window, 2, { scrollTo: { y: ".nav_bar" }, ease: Power2.easeOut });
        }
    });
}

// stop ducks animation play if its not in view port start
let isInViewport = function(elem){
    let bounding = elem.getBoundingClientRect(),
    elHeight = elem.clientHeight * 0.8,
    elWidth = elem.clientWidth * 0.8;
    return(
        bounding.top + elHeight >= 0 &&
        bounding.left + elWidth >= 0 &&
        bounding.bottom <= (window.innerHeight || document.documentElement.clientHeight) &&
        bounding.right <= (window.innerWidth || document.documentElement.clientWidth)
    );
}

if(document.body.classList.contains('home') && desktopAnimationCnt){
    window.addEventListener('load', () => {
        let throttledTopAnimationPauseResume = o.throttle(
            function(element){
                if(!document.body.classList.contains('mobile_menu_active')
                        && !document.body.classList.contains('dropdownmenu_active') 
                        && isInViewport(element)){
                        if(wavesAndDucksTl.isActive()){
                            wavesAndDucksTl.resume();
                        }else{
                            wavesAndDucksTl.play();
                        }
                }else if(!document.body.classList.contains('mobile_menu_active')
                            && !document.body.classList.contains('dropdownmenu_active') 
                            && !isInViewport(element)){
                                wavesAndDucksTl.paused() ? null : wavesAndDucksTl.pause();
                }
        }, 300, { leading: true, trailing: true });
        window.addEventListener('scroll', () => {
            throttledTopAnimationPauseResume(desktopAnimationCnt);  
        }); 
    });   
}
// stop ducks animation play if its not in view port end
