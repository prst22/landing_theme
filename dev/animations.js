import { Expo } from "gsap/all";

const desktopAnimationCnt = document.querySelector('.desktop_top_animation'),
mobileAnimationCnt = document.querySelector('.mobile_top_animation');
let ducksAnimation,
mobileAnimation,
textAnimation;

if(desktopAnimationCnt) ducksAnimation = TweenMax.to(desktopAnimationCnt, 1.1, { autoAlpha: 1, ease: Power1.easeInOut, onStart: createDucksTimeline, onComplete: playDucksTimeline, immediateRender: false, paused: true });
if(mobileAnimationCnt) mobileAnimation = TweenMax.to(mobileAnimationCnt, 1.1, { autoAlpha: 1, ease: Power1.easeInOut, immediateRender: false });

if(desktopAnimationCnt){  
    ducksAnimation.play();
}

if(mobileAnimationCnt){
    window.addEventListener('DOMContentLoaded', () => {
        textAnimation = new TimelineLite()
            .to('#text_anim', 2, { text: 'Gallery', delay: 1.1, ease: Power0.easeNone })
            .to('.hero_body__text', 1.1, { text: { value: 'Wordpress', newClass: 'text_bright', padSpace:true }, ease: Power0.easeNone }, "-=1");

    });
}

function createDucksTimeline(){
    const stars = document.getElementById('stars').childNodes;

    wavesAndDucksTl = new TimelineLite;

    wavesAndDucksTl.add(TweenMax.fromTo('#duck1', 3, { rotationX: -95, transformOrigin: "bottom 0%"}, { rotation: 10, ease: Power1.easeInOut, repeat: -1, yoyo: true, immediateRender: false, force3D: true }), 0);
    wavesAndDucksTl.add(TweenMax.to('#duck1', 1, { y: -110, ease: Power1.easeInOut, repeat: -1, yoyo: true }));

    wavesAndDucksTl.add(TweenMax.fromTo('#duck2', 2, { rotationX: -40, transformOrigin: "bottom 0%"}, { rotation: 18, ease: Power1.easeInOut, repeat: -1, yoyo: true, immediateRender: false, force3D: true }), 0);
    wavesAndDucksTl.add(TweenMax.to('#duck2', 1.4, { y: 90, ease: Power2.easeInOut, repeat: -1, yoyo: true }));

    wavesAndDucksTl.add(TweenMax.fromTo('#duck3', 2.5, { rotation: -12, transformOrigin: "bottom 0%"}, { rotation: 15, ease: Power1.easeInOut, repeat: -1, yoyo: true, delay: -0.7, immediateRender: false, force3D: true }), 0);
    wavesAndDucksTl.add(TweenMax.to('#duck3', 1, { y: -90, ease: Power1.easeInOut, repeat: -1, yoyo: true }), 0);

    //wave animation
    wavesAndDucksTl.add(TweenMax.fromTo('#wave1', 2, { x: '-900' }, { x: '300', ease: Power1.easeInOut, repeat: -1, yoyo: true, immediateRender: false, force3D: true }), 0);
    wavesAndDucksTl.add(TweenMax.fromTo('#wave2', 3, { x: '-800' }, { x: '300', ease: Power1.easeInOut, repeat: -1, yoyo: true, delay: -1.7, immediateRender: false, force3D: true }), 0);
    wavesAndDucksTl.add(TweenMax.fromTo('#wave3', 4, { x: '-600' }, { x: '300', ease: Power1.easeInOut, repeat: -1, yoyo: true, delay: -1, immediateRender: false, force3D: true }), 0);

    //Star animation

    stars.forEach((element) => {
        wavesAndDucksTl.add(TweenMax.to(element, (Math.random() * 1.5) + 1, { scale: 0, transformOrigin:"50% 50%", yoyo: true, repeat: -1, delay: -Math.random() * 2, immediateRender: false }), 0);
    });

    wavesAndDucksTl.pause(2, true);
    return wavesAndDucksTl;
}

export let wavesAndDucksTl;

function playDucksTimeline(){
    let bodyClasses = document.body.classList;
    if(!desktopAnimationCnt) return
    if(bodyClasses.contains('mobile_menu_active') || bodyClasses.contains('dropdownmenu_active')){
        wavesAndDucksTl.pause(2, true);
    }else{
        wavesAndDucksTl.play(2);
    }  
}

let animate = {
    x: 0,
    y: 0,
    rotation3d: Object.create(null),
    getTopPosition: function(element){
        let xPosition = 0,
        yPosition = 0;
        while(element){
            xPosition += element.offsetLeft;
            yPosition += element.offsetTop;
            element = element.offsetParent;
        }
        return { x: xPosition, y: yPosition };
    },
    handleMouseLeave: function(e){
        let caption = e.currentTarget.querySelector('.gallery__caption');
        if(caption){
            TweenMax.to(caption, 0.3, { opacity: 0, scale: 1.2, delay: 0, ease: Power0.easeNone }); 
        }
        TweenLite.to('.gallery_item', 1, { rotation: 0, rotationY: 0, ease: Power0.easeNone, delay: 1 }); 
    },
    handleMouseEnter: function(e){
        let xPosition = this.getTopPosition(e.currentTarget).x,
        yPosition = this.getTopPosition(e.currentTarget).y,
        caption = e.currentTarget.querySelector('.gallery__caption');
        this.x = xPosition;
        this.y = yPosition;
        if(caption){
            TweenMax.to(caption, 0.6, { opacity: 1, scale: 1, delay: 0, ease: Expo.easeOut }); 
        }
    },
    handleMouseOver: function(e){
        let xPosition = this.getTopPosition(e.currentTarget).x,
        yPosition = this.getTopPosition(e.currentTarget).y;
        this.x = xPosition;
        this.y = yPosition;
    },
    handleMouseMove: function(e){        
        let galleryThumbWidth = document.querySelector('.gallery_item').offsetWidth,
        galleryThumbHeight = document.querySelector('.gallery_item').offsetHeight,
        thumbOffsetLeft = this.x,
        thumbOffsetTop = this.y,
        mouseX = e.pageX - thumbOffsetLeft - galleryThumbWidth/2,
        mouseY = e.pageY - thumbOffsetTop - galleryThumbHeight/2,
        rotX = (mouseX / galleryThumbWidth) * 6,
        rotY = (mouseY / galleryThumbHeight) * -2;

        if('isActive' in this.rotation3d){
            if(this.rotation3d.isActive()){
                this.rotation3d.kill();
                this.rotation3d = new TweenMax.to(e.currentTarget, 0.3, { onStart: function(){ this.handleMouseOver(e)}.call(this, e), rotation: rotY, rotationY: rotX,  ease: Power0.easeNone } ); 
            }else{
                this.rotation3d.kill();
                this.rotation3d = new TweenMax.to(e.currentTarget, 0.3, { onStart: function(){ this.handleMouseOver(e)}.call(this, e), rotation: rotY, rotationY: rotX, ease: Power0.easeNone } );
            }
        }else{
            this.rotation3d = new TweenMax.to(e.currentTarget, 0.4, { onStart: function(){ this.handleMouseOver(e)}.call(this, e), rotation: -rotX, rotationY: rotX, ease: Power0.easeNone });
        }
    },
    initGlare: function(e){
        TweenMax.to(e.currentTarget, 0.5, { opacity: 1, ease: Power0.easeNone }); 
    },
    removeGlare: function(e){
        TweenMax.to(e.currentTarget, 0.5, { opacity: 0, ease: Power0.easeNone }); 
    }
};

export default animate;
