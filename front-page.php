<?php get_header(); ?>
    
<?php

    $detect = new Mobile_Detect();

    if ($detect->isMobile() && !$detect->isTablet()){ 
        get_template_part( 'template_parts/top-mobile-showcase' );
    }
    if( $detect->isMobile() && $detect->isTablet() ){
        get_template_part( 'template_parts/top-desktop-showcase' );
    }
    if( !$detect->isMobile() && !$detect->isTablet() ){
        get_template_part( 'template_parts/top-desktop-showcase' );
    }
    
?>

<?php
    
    $gallery_args = array(
        'post_status' => 'publish',
        'no_found_rows' => true, 
        'posts_per_page' => 3,
        'update_post_meta_cache' => false, 
        'update_post_term_cache' => false, 
        'fields' => 'ids',
        'tax_query' => array(
            array(                
                'taxonomy' => 'post_format',
                'field' => 'slug',
                'terms' => array( 
                    'post-format-gallery',
                ),
                'operator' => 'IN'
            )
        )
    );

    $gallery_query = new WP_Query($gallery_args);

    if ( $gallery_query->have_posts() ):
    
            while ( $gallery_query->have_posts() ):

                $gallery_query->the_post();	
                get_template_part( 'template_parts/index', 'gallery' );

            endwhile; 
            wp_reset_postdata();
        endif; 
    ?>

    <?php $gallery_query->rewind_posts(); ?>

    <?php
    
        $video_args = array(
            'post_status' => 'publish',
            'no_found_rows' => true, 
            'update_post_meta_cache' => false, 
            'update_post_term_cache' => false, 
            'posts_per_page' => 8,
            'fields' => 'ids',
            'tax_query' => array(
                array(                
                    'taxonomy' => 'post_format',
                    'field' => 'slug',
                    'terms' => array( 
                        'post-format-video',
                    ),
                    'operator' => 'IN'
                )
            )
        );

        $videos_query = new WP_Query($video_args);

        if ( $videos_query->have_posts() ): ?>

        <section class="section section--tb angle">
            <div class="container">
                <div class="columns">
                    <?php if(get_theme_mod('front_page_video_section_title')): ?>
                        <h1 class="column col-12 text_center">
                            <a href="<?php echo get_page_link(get_page_by_path('all-videos')->ID ); ?>"><?php echo get_theme_mod('front_page_video_section_title'); ?></a>
                        </h1>
                    <?php endif; ?>
                    <?php  
                        while ( $videos_query->have_posts() ):

                            $videos_query->the_post();
                            get_template_part( 'template_parts/index', 'video' );

                        endwhile; ?>
                    <div class="column col-12 view_all">
                        <a href="<?php echo get_page_link(get_page_by_path('all-videos')->ID ); ?>" class="btn btn-primary wide_btn">All video galleries</a>
                    </div>
                </div>
            </div>
        </section>   

        <?php  
            wp_reset_postdata();

            endif;
        ?>

    <?php
        
        $posts_args = array(
            'post_status' => 'publish',
            'no_found_rows' => true, 
            'update_post_meta_cache' => false, 
            'update_post_term_cache' => false, 
            'fields' => 'ids',
            'posts_per_page' => 10,
            'tax_query' => array(
                array(                
                    'taxonomy' => 'post_format',
                    'field' => 'slug',
                    'terms' => array( 
                        'post-format-video',
                        'post-format-gallery'
                    ),
                    'operator' => 'NOT IN'
                )
            )
        );

        $standard_posts_query = new WP_Query($posts_args);
        
        if ( $standard_posts_query->have_posts() ):?>
            <section class="section section--tb angle standard_aside">
                <div class="container">
                    <div class="columns">

                        <?php if(get_theme_mod('front_page_blog_title')): ?>
                            <h1 class="column col-12 text_center">
                                <a href="<?php echo get_page_link( get_page_by_path('all-news')->ID ); ?>"><?php echo get_theme_mod('front_page_blog_title'); ?></a>
                            </h1>
                        <?php endif; ?>

                        <?php while ($standard_posts_query->have_posts()):

                            $standard_posts_query->the_post();
                                
                            if(get_post_format() === 'aside'){
                                get_template_part( 'template_parts/index', 'aside' );
                            }else{
                                get_template_part( 'template_parts/index',  'standard' );
                            }

                        endwhile; ?>
                        <div class="column col-12 view_all">
                            <?php if(get_theme_mod('front_page_blog_title')): ?>
                                <a href="<?php echo get_page_link( get_page_by_path('all-news')->ID ); ?>" class="btn btn-primary wide_btn"><?php echo 'All ' . get_theme_mod('front_page_blog_title'); ?></a>  
                            <?php else: ?>
                                <a href="<?php echo get_page_link( get_page_by_path('all-news')->ID ); ?>" class="btn btn-primary wide_btn">All posts</a>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </section>
        <?php endif; ?>

<?php get_footer(); ?>