<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo('charset'); ?>">
	<meta name="description" content="<?php bloginfo('description'); ?>">
	<title>
	    <?php bloginfo('name');?> |
	    <?php is_front_page() ? bloginfo('description') : wp_title(''); ?> 
	</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="author" content="Igor Barabash">
    <meta name="keywords" content="Landing wordpress theme">
	<link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_stylesheet_directory_uri(); ?>/apple-touch-icon.png">
	<link rel="icon" type="image/png" sizes="32x32" href="<?php echo get_stylesheet_directory_uri(); ?>/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="16x16" href="<?php echo get_stylesheet_directory_uri(); ?>/favicon-16x16.png">
	<link rel="manifest" href="<?php echo get_stylesheet_directory_uri(); ?>/site.webmanifest">
	<link rel="mask-icon" href="<?php echo get_stylesheet_directory_uri(); ?>/safari-pinned-tab.svg" color="#4c4c47">
	<meta name="msapplication-TileColor" content="#2d89ef">
	<meta name="theme-color" content="#ffffff">
	<?php wp_head(); ?>
	<style>
		.ls-blur-up-img,
		.mediabox-img {
			position: absolute;
			top: 0;
			left: 0;
			width: 100%;
			height: 100%;
			display: block;
		}
		.ls-blur-up-img {
			filter: blur(10px);
			opacity: 1;
			transition: opacity 1000ms, filter 1500ms;
		}
		.ls-blur-up-img.ls-inview.ls-original-loaded {
			opacity: 0;
			filter: blur(5px);
		}
		.top_showcase > .angle_top{
			display: block;
			position: absolute;
            height: 120px;
			top: 0;
			left: 0;
            right: 0;
            width: 100%;
            border-style: solid;
			border-width: 120px 100vw 0 0;
            border-color: #eee9e3 transparent transparent transparent;
            z-index: 20;
		}
		.angle::before{
			content: '';
            display: block;
            position: absolute;
			height: 120px;
			top: -120px;
			left: 0;
            right: 0;
            width: 100%;
            border-style: solid;
			border-width: 0 0 120px 100vw;
            border-color: transparent transparent #4C4C47 transparent;
		}
		.angle::after{
			content: '';
			display: block;
			height: 120px;
			position: absolute;
			bottom: -120px;
			left: 0;
            right: 0;
            border-style: solid;
			border-width: 120px 0 0 100vw;
            border-color: #4C4C47 transparent transparent transparent;
		}
		.section--tb:nth-child(odd).angle::before{
			content: '';
			display: block;
			height: 120px;
			position: absolute;
			top: -120px;
			left: 0;
            right: 0;
            border-style: solid;
			border-width: 120px 0 0 100vw;
            border-color: transparent transparent transparent #eee9e3;
		}
		.section--tb:nth-child(odd).angle::after{
			content: '';
			display: block;
			height: 120px;
			position: absolute;
			bottom: -120px;
			left: 0;
            right: 0;
            border-style: solid;
			border-width: 120px 100vw 0 0;
            border-color: #eee9e3 transparent transparent transparent;
        }
        .top_showcase__inner.mobile_top_animation,
         .top_showcase__inner.desktop_top_animation{
            position: relative;
            opacity: 0;
            z-index: 1;
        }
	</style>
</head>

<body <?php body_class();?>>
	<div class="blur">
	<nav class="container nav_bar">
		<div class="columns">
			<div class="column col-sm-9 col-md-7 col-lg-3 col-xl-4 col-4">
				<header class="site_header_cnt__site_header">
					<a href="<?php echo home_url(); ?>" title="Go home" class="main_site_logo">
						<img src="<?php echo esc_url(get_template_directory_uri() . '/assets/logo.svg'); ?>" alt="<?php bloginfo('name'); ?>">
					</a>
				</header>
			</div>
			
            <div class="column col-sm-3 col-md-5 col-lg-9 col-xl-8 col-8">
                <div class="nav">
                    <?php if ( has_nav_menu( 'mobile_menu' ) ) : ?>
                        <div class="mobile_menu_toggle show-lg hide-xl">
                            <div class="mobile_menu_toggle__button">
                                <button class="btn btn-primary btn-lg btn-action btn_flex" id="mobile_menu_btn">
                                    <svg class="icon menu">
                                        <use xlink:href="<?php echo get_template_directory_uri(); ?>/symbol-defs.svg#menu"></use>
                                    </svg>
                                </button>
                            </div>
                        </div>
                    <?php endif; ?>
                    <?php if ( has_nav_menu( 'main_menu' ) ) : ?>
                        <?php
                            wp_nav_menu( 
                                array(
                                    'menu'              => 'Main menu',
                                    'theme_location'    => 'main_menu',
                                    'menu_class'        => 'top_menu',
                                    'depth'             => 2,          
                                    'container_class'   => 'nav__inner hide-lg',
                                    'walker'            => new Walkernav()           
                                )
                            );
                        ?>
                    <?php endif; ?>
                </div>
            </div>
			
		</div>
	</nav>	
	
	<div class="site_wrap">
		<div class="site_wrap__inner">