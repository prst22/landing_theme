<?php get_header(); ?>

	<main class="container grid-xl main archive_page">
        <div class="columns">

		<?php
			if ( have_posts() ) {

				echo '<div class="column col-12">';
				echo '<header>';
				
					the_archive_title( '<h1 class="archive_page__title">', '</h1>' );
			
			    echo '</header>';
			    if(category_description()){
			    	echo category_description();
			    }
			    echo '</div>';

				while ( have_posts() ) : the_post();

					get_template_part( 'template_parts/index', 'standard' );

				endwhile;

				if($wp_query->max_num_pages > 0): ?>	
					
					<div class="column col-12">
						<div class="pagination">
                            <?php 
                                echo paginate_links(
									array(
										'prev_next'         => false,
										'type'              => 'list',
										'end_size'          => 2,
										'mid_size'          => 2,
									)
                                ); 
                            ?>
						</div>
					</div>
					
				<?php endif; ?>

			<?php } else { ?>

				<h1>Nothing to show(</h1>

			<?php }
		?>

		</div>
	</main>

<?php get_footer(); ?>