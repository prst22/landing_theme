<?php get_header(); ?>
<?php 
    $content_class_width = (is_single() && get_post_format() === 'gallery') || (is_single() && get_post_format() === 'video') ? 'grid-lg' : 'grid-xl'; 
?>
                   
<?php while ( have_posts() ) : the_post(); ?>

    <?php 
        if(is_single() && get_post_format() === 'video'): ?>

        <main class="single_post">
            <div class="container <?php echo (!get_post_format() ? '' : $content_class_width); ?>">
                <div class="columns">
                    <div class="column col-12">
                        <?php get_template_part( 'template_parts/content', 'video' ); ?>
                    </div>
                </div>
            </div>    
        </main>    

        <?php elseif(is_single() && get_post_format() === 'aside'): ?>

        <main class="single_post">
            <div class="container <?php echo (!get_post_format() ? '' : $content_class_width); ?>">
                <div class="columns">
                    <div class="column col-12">
                        <?php get_template_part( 'template_parts/content', 'aside' ); ?>
                    </div>
                </div>
            </div>  
        </main>  

        <?php elseif(is_single() && get_post_format() === 'gallery'): ?>

        <main class="single_post">
            <div class="container <?php echo (!get_post_format() ? '' : $content_class_width); ?>">
                <div class="columns">
                    <div class="column col-12">
                        <?php get_template_part( 'template_parts/content', 'gallery' ); ?>
                    </div>
                </div>
            </div>  
        </main>  

        <?php else: ?>

            <?php get_template_part( 'template_parts/content', 'standard' ); ?>

        <?php endif; ?>  

<?php endwhile; ?>

<?php get_footer(); ?>