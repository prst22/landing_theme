<?php /* Template Name: All galleries */ ?>

<?php get_header(); ?>

<?php
    $paged = ( get_query_var( 'paged' ) ) ? absint( get_query_var( 'paged' ) ) : 1;
    
    $gallery_args = array(
        'post_status' => 'publish',
        'fields' => 'ids',
        'paged' => $paged,
        'tax_query' => array(
            array(                
                'taxonomy' => 'post_format',
                'field' => 'slug',
                'terms' => array( 
                    'post-format-gallery'
                ),
                'operator' => 'IN'
            )
        )
    );
?>
    <main class="container grid-xl main">
        <div class="columns">
        <?php 

            $gallery_query = new WP_Query($gallery_args);
        
            if ( $gallery_query->have_posts() ): ?>
                    <h1 class="column col-12 text_center">
                        <?php the_title(); ?>
                    </h1>
                <?php while ( $gallery_query->have_posts() ):

                    $gallery_query->the_post();
                    
                    if(get_post_format() === 'gallery'){
                        get_template_part( 'template_parts/index', 'gallery' );
                    }

                ?>    
                <?php endwhile; ?>

            <?php wp_reset_postdata(); ?> 
                        
            <?php endif; ?>

            <?php if( $gallery_query->max_num_pages > 0 ): ?>
                <div class="column col-12">
                    <div class="pagination">
                        <?php echo paginate_links(
                            array(
                                'prev_next'     => false,
                                'total'         => $gallery_query->max_num_pages,
					            'current'       => $paged,
                                'type'          => 'list',
                                'end_size'      => 2,
                                'mid_size'      => 2
                                )
                            ); 
                        ?>
				    </div>
                </div>
            <?php endif; ?>
            
        </div>
    </main>
<?php get_footer(); ?>