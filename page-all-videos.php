<?php /* Template Name: All videos */ ?>

<?php get_header(); ?>

<?php
    $paged = ( get_query_var( 'paged' ) ) ? absint( get_query_var( 'paged' ) ) : 1;
    
    $video_args = array(
        'post_status' => 'publish',
        'fields' => 'ids',
        'paged' => $paged,
        'tax_query' => array(
            array(                
                'taxonomy' => 'post_format',
                'field' => 'slug',
                'terms' => array( 
                    'post-format-video'
                ),
                'operator' => 'IN'
            )
        )
    );
?>
    <main class="container grid-xl main">
        <div class="columns">
        <?php 

            $videos_query = new WP_Query($video_args);
        
            if ( $videos_query->have_posts() ): ?>
                <?php if(get_theme_mod('front_page_video_section_title')): ?>
                    <h1 class="column col-12 text_center">
                        <?php echo get_theme_mod('front_page_video_section_title'); ?>
                    </h1>
                <?php endif; ?>
                <?php while ( $videos_query->have_posts() ):

                    $videos_query->the_post();
                    
                    if(get_post_format() === 'video'){
                        get_template_part( 'template_parts/index', 'video' );
                    }

                ?>    
                <?php endwhile; ?>

            <?php wp_reset_postdata(); ?> 
                        
            <?php endif; ?>

            <?php if($videos_query->max_num_pages > 0): ?>
                <div class="column col-12">
                    <div class="pagination">
                        <?php echo paginate_links(
                            array(
                                'prev_next'     => false,
                                'total'         => $videos_query->max_num_pages,
					            'current'       => $paged,
                                'type'          => 'list',
                                'end_size'      => 2,
                                'mid_size'      => 2
                                )
                            ); 
                        ?>
				    </div>
                </div>
            <?php endif; ?>
            
        </div>
    </main>
<?php get_footer(); ?>