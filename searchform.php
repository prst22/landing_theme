<form method="get" class="searchform form-inline search_form" action="<?php echo esc_url( home_url( '/' ) ); ?>">
	<div class="input-group">
	  	<input type="text" class="form-input input-lg search_field" name="s" placeholder="<?php esc_attr_e( 'Search' ); ?>" title="search" size="22">
	  	<button class="btn btn-primary btn-lg input-group-btn btn_flex" type="submit" name="submit">
            <svg class="icon search_icon">
                <use xlink:href="<?php echo get_template_directory_uri(); ?>/symbol-defs.svg#search"></use>
            </svg>
        </button>
	</div>
</form>