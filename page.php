<?php get_header(); ?>
    <main class="container grid-xl main">
        <div class="columns">
            <div class="column col-12">
                <div class="single_page">
                    <?php while ( have_posts() ) : the_post(); ?>
                   
                        <header class="single_page__heading">
                            <h1 class="heading_title"><?php the_title(); ?></h1>
                        </header>
                        <div class="content_here">
                            <?php  the_content();  ?>
                        </div>
                
                    <?php endwhile; ?>
                </div>
            </div>
        </div>
    </main>
<?php get_footer(); ?>