<?php
	get_header();
?>
<main class="container grid-xl main">

	<div class="columns main__inner">
		<?php

			if (have_posts()) : while ( have_posts() ) : the_post();
			
				get_template_part( 'template_parts/index', 'standard' );
									
				endwhile;

				else: 
                    get_template_part( 'template-parts/index','nopost' );

			 endif; ?>	

			<div class="column col-12">
				<div class="pagination">
					
					<?php
						echo paginate_links(
							array(
								'prev_next'         => false,
								'type'              => 'list',
								'end_size'          => 2,
								'mid_size'          => 2,
							)
						); 
					?>
					
				</div>
			</div>
	</div>
</main>

<?php		
	get_footer();