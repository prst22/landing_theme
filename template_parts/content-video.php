<div class="single_video_gallery">
    <header class="heading">
        <h1 class="heading_title">
            <?php the_title(); ?>
        </h1>
    </header>
    <div class="content">
        <?php the_content(); ?>
    </div>
</div>