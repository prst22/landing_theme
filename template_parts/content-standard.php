<main class="single_post mt_0">
    <div class="single_standard">
        <?php if(has_post_thumbnail()): 
            $low_src = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()), 'landing-thumnail')[0];
            $srcset = wp_get_attachment_image_srcset(get_post_thumbnail_id(get_the_ID()), 'landing-thumnail-4x');
        ?>
        <header class="heading heading--with_picture">
            <figure class="heading__picture">
                <img 
                    src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                    data-lowsrc="<?php echo esc_attr($low_src); ?>"
                    data-srcset="<?php echo esc_attr($srcset); ?>" 
                    class="mediabox-img lazyload"
                    sizes="auto"
                    alt="<?php echo esc_attr(get_the_title()); ?>">
            </figure>
            <div class="standard_post_info">
                <h1 class="heading_title h2">
                    <?php the_title(); ?>
                </h1>
                <div>
                    <small class="category_cnt">
                        <?php landing_print_categories(); ?>
                    </small>
                </div>
                <div>
                    <small class="time after_img">
                        <time><?php echo get_the_date(); ?></time>
                    </small>
                </div>
            </div>
            <div class="standard_picture_grad"></div>
        </header>

        <?php else: ?>

        <header class="container grid-xl heading">
            <div class="columns">
                <div class="column col-12">
                    <header class="heading">
                        <h1 class="heading__title">
                            <?php the_title(); ?>
                        </h1>
                        <div>
                            <small class="category_cnt">
                                <?php landing_print_categories(); ?>
                            </small>
                        </div>
                        <small class="time">
                            <time><?php echo get_the_date(); ?></time>
                        </small>
                    </header>    
                </div>
            </div>
        </header>   
        <?php endif; ?>
        <div class="container grid-xl">
            <div class="columns">
                <div class="column col-12">
                    <div class="content">
                        <?php the_content(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>