<article <?php post_class( array('news', 'column', 'col-12', 'standard') ); ?>>
    <div class="news__inner clearfix">

        <div class="news_excerpt">
            <?php if(has_post_thumbnail()): ?><!-- has thumb start -->
    
            <?php
                $low_src = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()), 'landing-thumnail')[0];
                $srcset = wp_get_attachment_image_srcset(get_post_thumbnail_id(get_the_ID()), 'landing-thumnail');
            ?>

            <figure class="thumbnail_cnt">
                <a href="<?php the_permalink(); ?>" title="Link to <?php the_title_attribute(); ?>" class="thumbnail_cnt__link">
                    <img 
                        src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                        data-lowsrc="<?php echo esc_attr($low_src); ?>"
                        data-srcset="<?php echo esc_attr($srcset); ?>" 
                        class="thumbnail mediabox-img lazyload"
                        sizes="(max-width: 960px) 510px, (min-width: 961px) 510px, (min-width: 1281px) 300px, 1200px"
                        alt="<?php echo esc_attr(get_the_title()); ?>">
                </a>   
            </figure> 

            <?php endif; ?><!-- has thumb end -->
            
            <header class="heading">
                <h2 class="standard_heading">
                    <a href="<?php the_permalink() ?>" title="Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a>
                </h2>
                <?php 
                if(get_post_format() !== 'aside' && get_post_format() !== 'gallery' && get_post_format() !== 'video' && (get_post_type() !== 'page')): ?>
                <div>
                    <small class="category_cnt">
                        <?php landing_print_categories(); ?>
                    </small>
                </div>
                <small class="time after_img">
                    <time>
                        <?php echo get_the_date(); ?>
                    </time>
                </small>
                <?php endif; ?>

                <?php if(get_post_format() === 'aside'): ?>
                <small class="time">
                    <time>
                        <?php echo get_the_date(); ?>
                    </time>
                </small>
                <?php endif; ?>
            </header>

            <?php the_excerpt(); ?>
        </div>
        
    </div>
</article>