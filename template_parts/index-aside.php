<article <?php post_class( array('column', 'col-12', 'aside') ); ?>>
    <header class="heading">
        <h2 class="heading_title_aside">
            <a href="<?php the_permalink() ?>" title="Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a>
        </h2>
        <div class="time">
            <small>
                <time>
                    <?php echo get_the_date(); ?>
                </time>
            </small>
        </div>
    </header>
    <div>
        <?php the_content(); ?>
    </div>
</article>