<?php if(has_post_thumbnail()): ?>

<?php
    $low_src = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()), 'landing-thumnail')[0];
    $srcset = wp_get_attachment_image_srcset(get_post_thumbnail_id(get_the_ID()), 'landing-thumnail');
?>

<div class="column col-3 col-xl-4 col-md-6 col-sm-12 gallery_item_outer">
    <div class="gallery_item">
        <a href="<?php the_permalink(); ?>" class="gallery gallery_item__link">
            <img 
                src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                data-lowsrc="<?php echo esc_attr($low_src); ?>"
                data-srcset="<?php echo esc_attr($srcset); ?>"
                sizes="(max-width: 840px) 510px, (min-width: 841px) and (max-width: 1400px) 300px, (min-width: 1401px) and (max-width: 2200px) 510px, 1200px" 
                class="gallery__img mediabox-img lazyload"
                alt="<?php echo esc_attr(get_the_title()); ?>">
            <div class="gallery__glare_1 glare"></div>
            <div class="gallery__glare_2 glare"></div>
            <div class="gallery__glare_3 glare"></div>
            <div class="gallery__glare_4 glare"></div>
            <div class="gallery__caption">
                <h5 class="caption_header">
                    <?php the_title(); ?>
                </h5>
            </div>
            
            <div class="gallery__video_icon_cnt">
                <svg class="icon">
                    <use xlink:href="<?php echo get_template_directory_uri(); ?>/symbol-defs.svg#play"></use>
                </svg>
            </div>
        </a>
        
    </div>
</div>
<?php endif; ?>
            
      