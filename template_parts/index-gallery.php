<?php 
    global $gallery_query;
    $current_post = get_post(get_the_ID());  
    $attachments = landing_get_gallery_attacment_ids($current_post);
    $captions_array = landing_get_all_gallery_captions($current_post);
    $sizes = is_front_page() ? '(max-width: 840px) 510px, (min-width: 841px) and (max-width: 1400px) 300px, (min-width: 1401px) and (max-width: 2200px) 510px, 1200px' : '(max-width: 840px) 510px, (min-width: 841px) and (max-width: 2200px) 300px, 510px';
    if(is_front_page() && $attachments): ?>

    <section class="section section--tb angle">
        <div class="container">
            <div class="columns">
    <?php endif; ?>

        <?php if($attachments): ?>

            <h1 class="column col-12 <?php echo (is_front_page() ? 'text_center' : ''); ?>">
                <a href="<?php the_permalink(); ?>">
                    <?php the_title(); ?>
                </a>
            </h1>

            <?php 
                foreach($attachments as $key => $attachment_id): 
                    $low_src = wp_get_attachment_image_src($attachment_id, 'landing-thumnail')[0];
                    $srcset = wp_get_attachment_image_srcset($attachment_id, 'landing-thumnail');
                    $image_caption = wp_get_attachment_caption($attachment_id) ?? '';
            ?>
                <div class="column col-3 col-xl-4 col-md-6 col-sm-12 gallery_item_outer">
                    <div class="gallery_item">
                        <a data-gallery="<?php echo get_the_ID(); ?>" data-glightbox="title: <?php echo ($captions_array[$key] !== '' ? $captions_array[$key] : $image_caption); ?>" href="<?php echo wp_get_attachment_image_src($attachment_id, 'landing-thumnail-4x')[0]; ?>" class="gallery gallery_item__link gallery_item__link--image_gallery glightbox">
                            <img 
                                src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                                data-lowsrc="<?php echo esc_attr($low_src); ?>"
                                data-srcset="<?php echo esc_attr($srcset); ?>"
                                sizes="<?php echo $sizes; ?>" 
                                class="gallery__img mediabox-img lazyload"
                                alt="<?php echo esc_attr($image_caption); ?>">
                            <div class="gallery__glare_1 glare"></div>
                            <div class="gallery__glare_2 glare"></div>
                            <div class="gallery__glare_3 glare"></div>
                            <div class="gallery__glare_4 glare"></div>
                            <?php if(!empty($captions_array[$key]) || !empty($image_caption)): ?>
                                <div class="gallery__caption">
                                    <h4 class="caption_header">
                                        <?php echo ($captions_array[$key] !== '' ? $captions_array[$key] : $image_caption); ?>
                                    </h4>
                                </div>
                            <?php endif; ?>
                        </a>
                    </div>
                </div>
            <?php endforeach; ?>
                    
        <?php endif; ?>

        <?php if(is_front_page() && $attachments && ($gallery_query->current_post + 1) === $gallery_query->post_count ): ?>
            <div class="column col-12 view_all">
                <a href="<?php echo get_page_link(get_page_by_path('all-galleries')->ID ); ?>" class="btn btn-primary wide_btn">All galleries</a>
            </div>
        <?php endif; ?>

    <?php if(is_front_page() && $attachments): ?>
            </div>
        </div>
    </section>
    <?php endif; ?>