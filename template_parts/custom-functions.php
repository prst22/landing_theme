<?php
    // get all gallery attachments ids for guttenberg and old galleries start

	function landing_get_gallery_attacment_ids(object $post_obj){
		if(empty((array) $post_obj)){
			return false;
		}
		$post_attachment_ids;
		if (has_block('gallery', $post_obj->post_content)){
			$post_blocks = parse_blocks($post_obj->post_content);

			foreach($post_blocks as $image_ids_arr){
				if($image_ids_arr['blockName'] === 'core/gallery'){
					foreach($image_ids_arr['attrs']['ids'] as $id){
						$post_attachment_ids[] = $id;
					}
				}
			}

		}else{
			// gets the gallery info
			$gallery = get_post_gallery($post_obj->ID, false);
			// makes an array of image ids
			$ids_str = explode(",", $gallery['ids']);

			foreach($ids_str as $id){
				$post_attachment_ids[] = (int)$id;
			}
		}
		return $post_attachment_ids;
	}

	// get all gallery attachments ids for guttenberg and old galleries end

    //get gallery image caption start

	function get_all_gallery_captions(object $post_obj){
		if (!has_block('gallery', $post_obj->post_content)){
			return;
		}
		$wp_blocks = parse_blocks($post_obj->post_content);
		$captions = array();
		foreach($wp_blocks as $item){
		    if($item['blockName'] === 'core/gallery'){
				$gallery_li_items = trim(preg_replace('/<ul[^>]+\>/i', '', $item['innerHTML']));
				$gallery_items_arr = explode('</li>', $gallery_li_items);
				array_pop($gallery_items_arr);
				foreach($gallery_items_arr as $item){
					$captions[] = strip_tags($item);
				}
		    }
		}
		return $captions;
    }
    
    //get gallery image caption end

    //get all categories list with comas start

	function landing_print_categories(){
		$output = '';
		$categories = get_categories( 
				array(
				'orderby' => 'name',
				'order'   => 'ASC'
			) 
		);
		
		foreach( $categories as $category ) {
			$category_link = sprintf( 
				'<a href="%1$s" alt="%2$s" class="category_cnt__category_link">%3$s</a>',
				esc_url( get_category_link( $category->term_id ) ),
				esc_attr( sprintf( __( 'View all posts in %s', 'landing_theme' ), $category->name ) ),
				esc_html( $category->name )
			);
			
			$output.= "$category_link" . ",";
		} 
		$output = trim($output, ',');
		echo sprintf( esc_html__( 'Category: %s', 'landing_theme' ), $output );
    }
    
    //get all categories list with comas end