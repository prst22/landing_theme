<section class="top_showcase">
    <div class="angle_top"></div>
        <div class="loading"></div>
        <div class="top_showcase__inner mobile_top_animation">
            <div class="hero">
                <div class="hero_body">
                    <h1 class="hero_body__title text_center" id="text_anim"> 
                        potatosites.com
                    </h1>
                    <h3 class="hero_body__text text_center">Wordpress</h3>
                </div>
            </div>
        </div>
    </div>
</section>