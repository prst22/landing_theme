<?php
    if ( file_exists( dirname( __FILE__ ) . '/vendor/autoload.php' ) ):
        require_once dirname( __FILE__ ) . '/vendor/autoload.php';
    endif;
    if ( file_exists( dirname( __FILE__ ) . '/inc/customizer.php' ) ):
        require get_template_directory() . '/inc/customizer.php';
    endif;
    if ( file_exists( dirname( __FILE__ ) . '/inc/custom-functions.php' ) ):
	    require get_template_directory() . '/inc/custom-functions.php';
    endif;

    if ( file_exists( dirname( __FILE__ ) . '/inc/mega_menu/Walkernav.php' ) ):
	    require get_template_directory() . '/inc/mega_menu/Walkernav.php';
    endif;
    if ( file_exists( dirname( __FILE__ ) . '/inc/mega_menu/megamenu-custom-fields.php' ) ):
	    require get_template_directory() . '/inc/mega_menu/megamenu-custom-fields.php';
    endif;
   
	//add theme support start

	function theme_setup(){
		load_theme_textdomain('landing_theme');

		// Adding menus to wp controll pannel
		register_nav_menus(
			array(
				'main_menu' =>__('Main header menu'),
				'mobile_menu' =>__('Mobile menu'),
				'footer_menu' =>__('Footer Menu')
			)
		);

	    //post thum pictures
		add_theme_support('post-thumbnails');
		
	    add_image_size('landing-thumnail', 100, 56, array('center', 'center'), true);  
	    add_image_size('landing-thumnail-2x', 300, 169, array('center', 'center'), true);  
		add_image_size('landing-thumnail-3x', 510, 287, array('center', 'center'), true);
		add_image_size('landing-thumnail-4x', 1200, 99999);
		
		add_theme_support(
			'html5',
			array(
				'search-form',
				'comment-form',
				'comment-list',
				'gallery',
				'caption',
			)
		);
	    add_theme_support('post-formats', array('video', 'gallery', 'aside'));   
	}
	add_action('after_setup_theme', 'theme_setup');

	//add theme support end

	//add js css start
	
	function add_styles_and_js(){
		wp_enqueue_style('style', get_theme_file_uri( '/css/main.min.css'));
		wp_enqueue_script('main_js', get_template_directory_uri() . '/js/main.min.js', array(), 1.0, true);
		if(current_user_can('administrator') || current_user_can('editor')){
			wp_localize_script('main_js', 'scrOffsetTop', array( 'offsetY' => 50 ));
		}
	}
	add_action('wp_enqueue_scripts', 'add_styles_and_js');
	
	//add js css end

	//controll excerpt start

	function set_excerpt_length(){
		return has_post_thumbnail() ? 55 : 70;
	}
	add_filter('excerpt_length', 'set_excerpt_length');

	function custom_excerpt($more){
        $format = '... <a href="%1$s" title="Link to %2$s" class="btn btn-primary btn-sm read_more btn_flex"><svg class="icon more"><use xlink:href="%3$s/symbol-defs.svg#more"></use></svg></a>';
        $more_str = sprintf($format, esc_url(get_permalink(get_the_ID())), get_the_title(get_the_ID()), esc_url(get_template_directory_uri(get_the_ID())));
        return $more_str;
	}
    add_filter( 'excerpt_more', 'custom_excerpt' );
    
    // function wpdocs_excerpt_more( $more ) {
    //     return sprintf( '<a href="%1$s" class="more-link">%1$s</a>',
    //           esc_url( get_permalink( get_the_ID() ) ),
    //           sprintf( __( 'Continue reading %s', 'wpdocs' ), '<span class="screen-reader-text">' . get_the_title( get_the_ID() ) . '</span>' )
    //     );
    // }
    // add_filter( 'excerpt_more', 'wpdocs_excerpt_more' );

	//controll excerpt end

	//widgets locations start

	function wpb_init_widgets($id){
		register_sidebar(array(
		    'name'=> 'footerSearch',
		    'id'=> 'footer-search',
		    'before_widget'=> '<div class="footer-search">',
		    'after_widget'=> '</div>'
		));
	}
	add_action('widgets_init', 'wpb_init_widgets');

	// widgets locations end

	// add responsive container to embeds
 
	function landing_embed_html( $html ){
	   return '<div class="video_content">' . $html . '</div>';
	}
	add_filter( 'embed_oembed_html', 'landing_embed_html', 10, 3 );
  
	// include the path to favicon start

	function add_favicon() {
	  	$favicon_url = get_template_directory_uri() . '/images/admin-favicon.ico';
		echo '<link rel="shortcut icon" href="' . $favicon_url . '" type="image/x-icon" sizes="32x32" />';
	}
	  
	// Now, just make sure that function runs when you're on the login page and admin pages  
	add_action('login_head', 'add_favicon');
	add_action('admin_head', 'add_favicon');

	// include the path to favicon end

	//include js file to change guttenberg start

	function landing_enqueue_block_assets(){
		wp_enqueue_script(
			'landing-gallery-block', get_theme_file_uri('/js/block.js'), ['wp-blocks']
		);
	}
	add_action('enqueue_block_editor_assets', 'landing_enqueue_block_assets');

	//include js file to change guttenberg start


	