/**
 * WordPress dependencies
 */

wp.hooks.addFilter(
'blocks.getSaveContent.extraProps',
'addLightBox',
addLightBoxGallerySaveElement
) 
function addLightBoxGallerySaveElement( props, attr ) {
if(attr.name !== 'core/gallery'){
    return props;
}
let imgCaption = [];
props.children.map((child, indx) => {

    child.props.children.props.children.map((link) => {
    if(link.props && !link.props.type){
        if(link.props.tagName === 'figcaption'){
            imgCaption.push(link.props.value);
        }
    }else{
        imgCaption.push('');
    }
    });

    child.props.children.props.children.map((link, index) => {
    if(link.type === 'a'){
        lodash.assign( link.props, {  
                'class': 'glightbox',
                'data-gallery': `gallery-${index}`,
                'data-glightbox': `title: ${imgCaption[indx]}`
            }
        );
    }
    });
    
});

return props;
}